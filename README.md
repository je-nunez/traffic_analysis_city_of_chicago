# Traffic Analysis in the City of Chicago

# WIP

This project is a *work in progress*. The implementation is *incomplete* and subject to change. The documentation can be inaccurate.

# Description

This is an analysis of the Traffic in the City of Chicago using Machine
Learning in `Julia`:

    http://juliastats.github.io/

For example, a simple model for the vehicular traffic can be (initially)
a linear model (with variables for the continuation of the flow of traffic
among directly contiguous intersections), so a `Generalized Linear Model`
can be useful to model the vehicular traffic (the `GLM` package in Julia):

    https://github.com/JuliaStats/GLM.jl

The underlying idea is a Linear Regression, so if by:

     tr(Point, Time)

designates the traffic at point `Point` at time `Time`, and
`S(Point)` is the `Set` of all other Points directly adjacent
to `Point` which may lead to `Point` driving (ie., the
`set S(Point)` denotes the immediate extremes of the simple
single-line street segments leading to `Point`), then:

    tr(Point,t) = sum [for p in S(Point)] (( Continuation(p, t-delta) * tr(p, t-delta)
                                             - Parked-in(p, t-delta, t)
                                             + Parked-out(p, t-delta, t))

where the variables:

* `Continuation(p, t-delta)` is the `probability` of the traffic at `p`
that continues (straight, or turns right, or left, or does a U-turn)
into `Point` at time `t-delta` (which incides at traffic `tr(Point, t)`
a little later, at time `t`, not immediately at time `t-delta`.) (So
this probability `Continuation(p, t-delta)` is a floating-point variable
between 0 and 1.)

* `Parked-in(p, t-delta, t)` are the numbers of cars that become
Parked during time interval `[t-delta, t]` in street segment `[p->Point]`;

* `Parked-out(p, t-delta, t)` the numbers of cars leaving parking and
entering into traffic in the street segment `[p->Point]` during time
interval `[t-delta, t]`; and

In this lineal model of the vehicular traffic, the samples `tr(Point, Time)`
are known, and the variables are the probabilities `Continuation(p, t-delta)`
of the probability of continuity of simple single-line traffic in each street
segment `[p->Point]`, and `Parked-in(p, t-delta, t)` and
`Parked-out(p, t-delta, t)`.

In other words, this model of the street traffic is a probabilistic`flow`
approximated by a `Locally Weighted Linear Regression`, like the one explained
here:

     http://mahout.apache.org/users/classification/locally-weighted-linear-regression.html

An example of the variables is to analyze the case when `Point` is `South-bound
traffic at N Clark St, just north at its intersection with W Fullerton Ave` (colored
in `Green` in the graph below), and the `Set S(this Point)` of immediate points
which may lead to `the Point mentioned before` (this `Set S(this Point)` of such
points are colored in `Red` in the graph below):

![Southbound-traffic at N Clark St north of W Fullerton Ave](chicago_traffic_analysis.png "Southbound-traffic at N Clark St north of W Fullerton Ave")

# Required Packages

You need to install the following packages in Julia:

    $ julia

        julia> Pkg.status()

        julia> Pkg.installed()

        julia> Pkg.add("HTTPClient")

        julia> Pkg.add("DataFrames")

        julia> Pkg.add("Distributions")

        julia> Pkg.add("GLM")


