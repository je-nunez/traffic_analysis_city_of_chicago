#!/usr/bin/env julia


# Use the HTTPClient and DataFrames pkgs to download and read
# the Chicago Traffic Count URL

using HTTPClient
using DataFrames

# For the Generalized Linear Regression
# The idea of the Linear Regression is that, if:
#
#     tr(Point, Time)
#
# designates the traffic at point "Point" at time "Time", and
# S(Point) is the "Set" of all other Points directly adjacent
# to "Point" which may lead to "Point" driving (street segments
# leading to "Point"), then:
#
#    tr(P,t) = sum-over-S(( Cont(p, t-delta) * tr(p, t-delta)
#                          - Parked-in(p, t-delta, t)
#                          + Parked-out(p, t-delta, t))
#
# where "Parked-in(p, t-delta, t)" are the numbers of cars
# Parked during time [t-delta, t] in street segment [p->Point],
# "Parked-out(p, t-delta, t)" the numbers of cars leaving parking
# in the street segment [p->Point], and "Cont(p, t-delta)" is
# the real-number coefficient between 0 and 1 of the traffic at
# "p" that continues (straight, or turns right, or left, or does
# a U-turn) into "Point" at time "t-delta" (which incides at
# traffic "tr(Point, t)" a little later, at time "t", not
# immediately at time "t-delta".)
#
# In this lineal model, tr(Point, Time) are known, and the variables
# are the coefficients "Cont(p, t-delta)" of continuity of traffic
# in each street segment [p->Point], and "Parked-in(p, t-delta, t)"
# and "Parked-out(p, t-delta, t)".


using Distributions
using GLM

# The Chicago Traffic Count URL

Chicago_Stats_Traffic_URL = "https://data.cityofchicago.org/api/views/4ndg-wq3w/rows.csv?accessType=DOWNLOAD"

# CVS filename to save

Chicago_Stats_Traffic_FName = "chicago_stats_traffic.csv"

# download the Chicago Traffic Count URL

chicago_stats_traffic_url = get(Chicago_Stats_Traffic_URL)

# Open the local download file

chicago_traffic_file = open(Chicago_Stats_Traffic_FName, "w")

# Write the URL to the local download file

write(chicago_traffic_file, chicago_stats_traffic_url.body.data)

close(chicago_traffic_file)

# Read the Chicago Traffic Count CVS

chicago_traffic_array = readtable(Chicago_Stats_Traffic_FName, header=true, separator=',')

println(chicago_traffic_array)

println(size(chicago_traffic_array))

